SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `system`
--

-- --------------------------------------------------------

--
-- Структура таблицы `contractor`
--

CREATE TABLE IF NOT EXISTS `contractor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contractor_id_uindex` (`id`),
  UNIQUE KEY `contractor_user_id_uindex` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `contractor`
--

INSERT INTO `contractor` (`id`, `name`, `email`, `phone`, `user_id`, `date_create`, `date_update`) VALUES
  (1, 'ООО "Тест 1"', 'test1@test.ru', '+79200000001', 10, '2017-09-19 21:57:21', NULL),
  (2, 'ООО "Тест 2"', 'test2@test.ru', '+79200000002', 34, '2017-09-20 22:53:41', NULL),
  (3, 'ООО "Тест 3"', 'test3@test.ru', '+79200000003', 20, '2017-09-20 22:56:29', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `artikul` varchar(64) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_id_uindex` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `name`, `artikul`, `brand`, `date_create`, `date_update`) VALUES
  (1, 'Apple iPhone SE 32Gb Rose Gold', '41945PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (2, 'Смартфон Apple iPhone SE 32Gb, цвет розовое золото (Rose Gold)', '41945PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (3, 'Apple iPhone SE 128Gb, Silver', '41952PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (4, 'Смартфон Apple iPhone SE 128Gb, цвет серебристый (Silver)', '41952PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (5, 'Apple iPhone 7 Plus 32GB Black', '38733PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (6, 'Apple iPhone 6S 32GB Silver', '39263PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (7, 'Apple iPhone 7 Plus 128GB Jet Black', '38731PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (8, 'Apple iPhone 6 32Gb Gold', '43805PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (9, 'Apple iPhone 7 Plus 256GB Jet Black', '38732PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (10, 'Apple iPhone 6S Plus 32GB Gold', '39505PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (11, 'Apple iPhone 7 128GB Jet Black', '38728PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (12, 'Apple iPhone 7 256GB Red', '41754PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (13, 'Apple iPhone 7 256GB Jet Black', '38729PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (14, 'Apple iPhone 7 128GB Rose Gold', '38727PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (15, 'Apple iPhone SE 32Gb Gold', '41946PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (16, 'Смартфон Apple iPhone SE 32Gb, цвет золотой (Gold)', '41946PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (17, 'Apple iPhone SE 128Gb, Space Grey', '41949PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (18, 'Смартфон Apple iPhone SE 128Gb, цвет серый космос (Space Gray)', '41949PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (19, 'Apple iPhone SE 128Gb, Rose Gold', '41951PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (20, 'Смартфон Apple iPhone SE 128Gb, цвет розовое золото (Rose Gold)', '41951PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (21, 'Apple iPhone 6S 32GB Gold', '39260PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (22, 'Apple iPhone 7 Plus 32GB Silver', '38739PHN', 'IPHONE', '2017-09-20 20:09:17', NULL),
  (23, 'Asus Zenfone 2 ZE551ML 32Gb Silver (в подарок Bright&Quick BQS-5065 Choice, Gold)', '43533PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (24, 'ASUS ZenFone 2 ZE551ML 32Gb Ram 4Gb Red', '33204PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (25, 'Asus Zenfone 2 ZE551ML 32GB Ram 4Gb, цвет красный (Red)', '33204PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (26, 'Asus Zenfone Go ZC500TG, Black', '37639PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (27, 'Asus Zenfone Go ZC500TG, цвет черный (Black)', '37639PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (28, 'Asus Zenfone 2 ZE551ML 32Gb, Gold', '33205PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (29, 'Asus Zenfone 2 ZE551ML 32GB Ram 4Gb, цвет золотой (Gold)', '33205PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (30, 'Asus Zenfone 3 Deluxe Dual Sim 64Gb, Silver', '41083PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (31, 'Asus Zenfone 3 Deluxe Dual Sim 64Gb, цвет серебристый (Silver)', '41083PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (32, 'Asus Zenfone Go ZC500TG, White', '34654PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (33, 'Asus Zenfone Go ZC500TG, цвет белый (White)', '34654PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (34, 'Asus Zenfone Selfie ZD551KL, White', '34652PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (35, 'Asus Zenfone Selfie ZD551KL, цвет белый (White)', '34652PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (36, 'Asus Zenfone Selfie 32GB ZD551KL, Silver', '36462PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (37, 'Asus Zenfone Selfie 32GB ZD551KL, цвет серебристый (Silver)', '36462PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (38, 'Asus Zenfone Go ZB452KG, White', '37350PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (39, 'Asus Zenfone Go ZB452KG, цвет белый (White)', '37350PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (40, 'Asus Zenfone Selfie 32GB ZD551KL, Gold', '36461PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (41, 'Asus Zenfone Selfie 32GB ZD551KL, цвет золотой (Gold)', '36461PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (42, 'Asus Zenfone 2 ZE551ML 32Gb, Silver', '33203PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (43, 'Asus Zenfone 2 ZE551ML 32Gb, цвет серебристый (Silver)', '33203PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (44, 'Asus Zenfone 2 Laser (ZE500KL) 16Gb, Silver', '34166PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (45, 'Asus Zenfone 2 Laser (ZE500KL) 16Gb, цвет белый (Silver)', '34166PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (46, 'Asus Zenfone Go ZB452KG Black', '36460PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (47, 'Asus Zenfone Go ZB452KG, цвет черный (Black)', '36460PHN', 'ASUS', '2017-09-20 20:09:20', NULL),
  (48, 'тест1', 'артикул1', 'бренд1', '2017-09-21 00:00:00', NULL),
  (49, 'тест2', 'артикул2', 'бренд2', '2017-09-21 00:00:00', NULL),
  (50, 'тест3', 'артикул3', 'бренд3', '2017-09-22 00:00:00', NULL),
  (51, 'тест4', 'артикул4', 'бренд4', '2017-09-22 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `rl_product_stock`
--

CREATE TABLE IF NOT EXISTS `rl_product_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rl_product_stock_id_uindex` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Дамп данных таблицы `rl_product_stock`
--

INSERT INTO `rl_product_stock` (`id`, `product_id`, `stock_id`, `quantity`) VALUES
  (2, 1, 1, 0),
  (3, 2, 1, 0),
  (4, 3, 1, 0),
  (5, 4, 1, 0),
  (6, 5, 1, 0),
  (7, 6, 1, 0),
  (8, 7, 1, 0),
  (9, 8, 1, 0),
  (10, 9, 1, 0),
  (11, 10, 1, 0),
  (12, 1, 2, 0),
  (13, 2, 2, 0),
  (14, 3, 2, 0),
  (15, 4, 2, 0),
  (16, 5, 2, 0),
  (17, 6, 2, 0),
  (18, 7, 2, 0),
  (19, 8, 2, 0),
  (20, 9, 2, 0),
  (21, 10, 2, 0),
  (22, 11, 3, 0),
  (23, 12, 3, 0),
  (24, 13, 3, 0),
  (25, 14, 3, 0),
  (26, 15, 3, 0),
  (27, 16, 3, 0),
  (28, 17, 3, 0),
  (29, 18, 3, 0),
  (30, 19, 3, 0),
  (31, 20, 3, 0),
  (32, 21, 4, 0),
  (33, 22, 4, 0),
  (34, 23, 4, 0),
  (35, 24, 4, 0),
  (36, 25, 4, 0),
  (37, 26, 4, 0),
  (38, 27, 4, 0),
  (39, 28, 4, 0),
  (40, 29, 4, 0),
  (41, 30, 4, 0),
  (42, 31, 5, 0),
  (43, 32, 5, 0),
  (44, 33, 5, 0),
  (45, 34, 5, 0),
  (46, 35, 5, 0),
  (47, 36, 5, 0),
  (48, 37, 5, 0),
  (49, 38, 5, 0),
  (50, 39, 5, 0),
  (51, 40, 5, 0),
  (52, 41, 6, 0),
  (53, 42, 6, 0),
  (54, 43, 6, 0),
  (55, 44, 6, 0),
  (56, 45, 6, 0),
  (57, 46, 6, 0),
  (58, 47, 6, 0),
  (59, 48, 3, 10),
  (60, 49, 3, 20),
  (61, 50, 1, 30),
  (62, 51, 1, 40);

-- --------------------------------------------------------

--
-- Структура таблицы `rl_user_contractor`
--

CREATE TABLE IF NOT EXISTS `rl_user_contractor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `contractor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rl_user_contractor_id_uindex` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Дамп данных таблицы `rl_user_contractor`
--

INSERT INTO `rl_user_contractor` (`id`, `user_id`, `contractor_id`) VALUES
  (1, 5, 1),
  (2, 5, 2),
  (3, 6, 2),
  (4, 7, 2),
  (5, 8, 2),
  (6, 9, 2),
  (8, 11, 2),
  (9, 12, 2),
  (10, 13, 2),
  (11, 1, 3),
  (12, 5, 3),
  (13, 6, 3),
  (14, 7, 3),
  (15, 8, 3),
  (16, 9, 3),
  (18, 11, 3),
  (19, 12, 3),
  (20, 13, 3),
  (21, 14, 3),
  (22, 15, 3),
  (23, 16, 3),
  (24, 17, 3),
  (25, 18, 3),
  (26, 19, 3),
  (28, 21, 3),
  (29, 22, 3),
  (30, 23, 3),
  (31, 24, 3),
  (32, 25, 3),
  (33, 26, 3),
  (34, 27, 3),
  (35, 28, 3),
  (36, 29, 3),
  (37, 30, 3),
  (38, 31, 3),
  (39, 32, 3),
  (40, 33, 3),
  (42, 24, 1),
  (43, 25, 1),
  (44, 26, 1),
  (45, 27, 1),
  (46, 28, 1),
  (47, 29, 1),
  (48, 30, 1),
  (49, 31, 1),
  (50, 32, 1),
  (51, 33, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contractor_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `region_code` int(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stock_id_uindex` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `stock`
--

INSERT INTO `stock` (`id`, `contractor_id`, `name`, `city`, `region`, `region_code`, `country`, `phone`, `email`, `date_create`, `date_update`) VALUES
  (1, 1, 'Склад 1', 'Москва', 'Москва', 99, 'Российская Федерация', '4999999', 'test1@test.ru', '2017-09-20 23:03:02', NULL),
  (2, 1, 'Склад 2', 'Нижний Новгород', 'Нижегородская область', 52, 'Российская Федерация', '5999999', 'test2@test.ru', '2017-09-20 23:04:37', NULL),
  (3, 2, 'Склад 3', 'Москва', 'Москва', 99, 'Российская Федерация', '4999999', 'test3@test.ru', '2017-09-20 23:03:02', NULL),
  (4, 2, 'Склад 4', 'Нижний Новгород', 'Нижегородская область', 52, 'Российская Федерация', '5999999', 'test4@test.ru', '2017-09-20 23:04:37', NULL),
  (5, 3, 'Склад 5', 'Москва', 'Москва', 99, 'Российская Федерация', '4999999', 'test5@test.ru', '2017-09-20 23:03:02', NULL),
  (6, 3, 'Склад 6', 'Нижний Новгород', 'Нижегородская область', 52, 'Российская Федерация', '5999999', 'test6@test.ru', '2017-09-20 23:04:37', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime DEFAULT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `patronymic` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id_uindex` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `email`, `date_create`, `date_update`, `firstname`, `lastname`, `patronymic`) VALUES
  (1, '', NULL, NULL, '2017-09-19 17:09:02', NULL, 'Петр', 'Судоров', 'Иванович'),
  (5, '', NULL, NULL, '2017-09-19 18:09:33', NULL, 'Илья', 'Петров', 'Дмитриевич'),
  (6, '', NULL, NULL, '2017-09-19 18:09:34', NULL, 'Александр', 'Детров', 'Сергеевич'),
  (7, '', NULL, NULL, '2017-09-19 18:09:34', NULL, 'Владимир', 'Сидоров', 'Михайлович'),
  (8, '', NULL, NULL, '2017-09-19 18:09:34', NULL, 'Дмитрий', 'Иванов', 'Андреевич'),
  (9, '', NULL, NULL, '2017-09-19 18:09:34', NULL, 'Илья', 'Сметров', 'Петрович'),
  (10, '', NULL, NULL, '2017-09-19 18:09:34', NULL, 'Дмитрий', 'Метров', 'Петрович'),
  (11, '', NULL, NULL, '2017-09-19 18:09:34', NULL, 'Илья', 'Сметров', 'Петрович'),
  (12, '', NULL, NULL, '2017-09-19 18:09:34', NULL, 'Владимир', 'Детров', 'Сергеевич'),
  (13, '', NULL, NULL, '2017-09-19 18:09:34', NULL, 'Дмитрий', 'Сметров', 'Михайлович'),
  (14, '', NULL, NULL, '2017-09-19 18:09:34', NULL, 'Илья', 'Детров', 'Петрович'),
  (15, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Иван', 'Кутилов', 'Михайлович'),
  (16, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Александр', 'Иванов', 'Сергеевич'),
  (17, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Константин', 'Сидоров', 'Николаевич'),
  (18, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Андрей', 'Мудилов', 'Андреевич'),
  (19, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Андрей', 'Детров', 'Петрович'),
  (20, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Андрей', 'Иванов', 'Андреевич'),
  (21, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Николай', 'Сидоров', 'Михайлович'),
  (22, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Дмитрий', 'Кутилов', 'Дмитриевич'),
  (23, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Иван', 'Метров', 'Николаевич'),
  (24, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Иван', 'Сидоров', 'Сергеевич'),
  (25, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Константин', 'Иванов', 'Дмитриевич'),
  (26, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Андрей', 'Кутилов', 'Михайлович'),
  (27, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Иван', 'Сметров', 'Михайлович'),
  (28, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Николай', 'Мудилов', 'Михайлович'),
  (29, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Александр', 'Иванов', 'Петрович'),
  (30, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Иван', 'Петров', 'Дмитриевич'),
  (31, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Андрей', 'Кутилов', 'Иванович'),
  (32, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Николай', 'Детров', 'Николаевич'),
  (33, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Илья', 'Мудилов', 'Михайлович'),
  (34, '', NULL, NULL, '2017-09-19 18:09:53', NULL, 'Иван', 'Сидоров', 'Николаевич');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
