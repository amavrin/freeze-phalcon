<?php
define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');
define('DI_PATH', APP_PATH . '/config/services.php');
define('CONFIG_PATH', APP_PATH . '/config/config.php');
define('LOADER_PATH', APP_PATH . '/config/loader.php');

$application = include_once DI_PATH;

// Handle the request
try {
    echo $application->handle()->getContent();
} catch (Exception $e) {
     echo "Exception: ", $e->getMessage();
}
