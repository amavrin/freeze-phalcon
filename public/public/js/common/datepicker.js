$(document).ready(function () {

    // datepickers init
// =================================================================
    if ($("#form-cost-edit-dat").length) {
        $("#form-cost-edit-dat").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            language: UserLang,
        });
    }
    if ($("#form-cost-create-date").length) {
        $("#form-cost-create-date").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            language: UserLang,
        });
    }
    if ($("#datepicker-filter").length) {
        $("#datepicker-filter .input-daterange").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            language: UserLang,
        });
    }
    if ($("#filterCreatedFrom").length) {
        $("#filterCreatedFrom").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            language: UserLang,
        });
    }
    if ($("#filterCreatedTo").length) {
        $("#filterCreatedTo").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            language: UserLang,
        });
    }
    if ($("#filterEditFrom").length) {
        $("#filterEditFrom").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            language: UserLang,
        });
    }
    if ($("#filterEditTo").length) {
        $("#filterEditTo").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            language: UserLang,
        });
    }
});
