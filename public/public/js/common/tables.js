/**
 * requires bootstrap tables

 <th>Id</th>
 <th>User Id</th>
 <th>Создан</th>
 <th>Обновлен</th>
 <th>Ref</th>
 <th>Email</th>
 <th>Дата рождения</th>
 <th>Полное имя</th>
 <th>Фамилия</th>
 <th>Имя</th>
 <th>Отчество</th>
 <th>Фамилия Латиницей</th>
 <th>Имя Латиницей</th>
 <th>Отчество Латиницей</th>
 <th>Кодовое слово</th>
 <th>ИНН</th>
 <th>СНИЛС</th>
 <th>ПОЛ</th>
 <th>Пометка на удаление</th>
 */

$(window).on('load', function () {


    if ($("#table-dt-selection-users").length) {
        $('#table-dt-selection-users').bootstrapTable();
    }

    if ($("#table-dt-selection-rluserrole").length) {
        $('#table-dt-selection-rluserrole').bootstrapTable();
    }

    if ($("#table-dt-selection-company").length) {
        var id = $("#table-dt-selection-company").attr('putid');
        $('#table-dt-selection-company').bootstrapTable(
            {
                queryParams: function (p) {
                    p.id = id;
                    return p;
                }
            }
        );
    }
    if ($("#company-bitrixTable").length) {
        var id = $("#company-bitrixTable").attr('putid');
        $('#company-bitrixTable').bootstrapTable(
            {
                queryParams: function (p) {
                    p.id = id;
                    return p;
                }
            }
        );
    }
    if ($("#company-employees-table").length) {
        var id = $("#company-employees-table").attr('putid');
        $('#company-employees-table').bootstrapTable(
            {
                queryParams: function (p) {
                    p.id = id;
                    return p;
                }
            }
        );
    }
    if ($("#table-dt-selection-company-fl").length) {
        var id = $("#table-dt-selection-company-fl").attr('putid');
        $('#table-dt-selection-company-fl').bootstrapTable(
            {
                queryParams: function (p) {
                    p.id = id;
                    return p;
                }
            }
        );
    }
    if ($("#table-dt-selection-company-empl").length) {
        var id = $("#table-dt-selection-company-empl").attr('putid');
        $('#table-dt-selection-company-empl').bootstrapTable(
            {
                queryParams: function (p) {
                    p.id = id;
                    return p;
                }
            }
        );
    }
    if ($("#table-dt-selection-company-position").length) {
        var id = $("#table-dt-selection-company-position").attr('putid');
        $('#table-dt-selection-company-position').bootstrapTable(
            {
                queryParams: function (p) {
                    p.id = id;
                    return p;
                }
            }
        );
    }
    if ( $("#company-employees-table").length ) {
        var id=$("#company-employees-table").attr('putid');
        $('#company-employees-table').bootstrapTable(
            {
                queryParams: function(p){
                    p.id = id;
                    return p;
                }
            }
        );
    }

    if ($("#table-dt-selection-company-structure").length) {
        var id = $("#table-dt-selection-company-structure").attr('putid');
        $('#table-dt-selection-company-structure').bootstrapTable(
            {
                queryParams: function (p) {
                    p.id = id;
                    return p;
                }
            }
        );
    }
    if ( $("#master-employees-table").length ) {
        var id=$("#master-employees-table").attr('putid');
        $('#master-employees-table').bootstrapTable(
            {
                resetOffset: true,
                queryParams: custom_query_master,
                onCheckAll: function () {
                    data = $('#master-employees-table').bootstrapTable('getData');
                    var tableSel = $('#master-selected-employees-table');
                    var selectSel = $('#selected-employees-hidden');
                    $.each(data, function() {
                        if (tableSel.find('tr td[data-flId='+  this.id +']').length <= 0 && selectSel.find('option[value='+ this.id  +']').length <= 0)
                        {
                            tableSel.append('<tr><td data-flId="' + this.id + '">' + this.fio + '</td></tr>');
                            selectSel.append('<option value="' + this.id + '" selected="selected">' + this.fio + '</option>');
                        }
                    });
                },
                onUncheckAll: function () {
                    data =  $('#master-employees-table').bootstrapTable('getData');
                    var tableSel = $('#master-selected-employees-table');
                    var selectSel = $('#selected-employees-hidden');
                    $.each(data, function() {
                        tableSel.find('tr td[data-flId='+  this.id +']').remove();
                        selectSel.find('option[value='+ this.id  +']').remove();
                    });
                },
                onLoadSuccess:function () {
                    data =  $('#master-employees-table').bootstrapTable('getData');
                    dataSelected = $('#master-selected-employees-table tbody tr td');
                    if (dataSelected.length > 0){
                        dataSelected.each(function(index, element){
                            $.each(data, function(i, val) {
                                if (this.id == $(element).attr('data-flid'))
                                {
                                    $("#master-employees-table").bootstrapTable("check", i);
                                }
                            });
                        });
                    }
                }
            }
        ).on('check.bs.table', function (e, row) {
            var tableSel = $('#master-selected-employees-table');
            var selectSel = $('#selected-employees-hidden');
            if (tableSel.find('tr td[data-flId='+  row.id +']').length <= 0 && selectSel.find('option[value='+ row.id  +']').length <= 0)
            {
                tableSel.append('<tr><td data-flId="' + row.id + '">' + row.fio + '</td></tr>');
                selectSel.append('<option value="' + row.id + '" selected="selected">' + row.fio + '</option>');
            }
        }).on('uncheck.bs.table', function (e, row) {
            var tableSel = $('#master-selected-employees-table');
            var selectSel = $('#selected-employees-hidden');
            tableSel.find('tr td[data-flId='+  row.id +']').remove();
            selectSel.find('option[value='+ row.id  +']').remove();
        });
    }

    if ($("#reference-table").length) {
        $('#reference-table').bootstrapTable(
            {
                queryParams: custom_queryReference,
                onLoadSuccess:function () {
                    $("#allFilterReference").niftyOverlay('hide');
                }
            }
        );
    }
    
    if ($("#advstat-table").length) {
        var id = $("#advstat-table").attr('putid');
        $('#advstat-table').bootstrapTable(
            {
                queryParams: custom_query
            }
        ).on('click', '.btn-advstat-delete', function () {
            var ticket = {};
            ticket.id = $(this).attr('data-ticket');
            bootbox.dialog({
                message: '<div class="media"><div class="media-body">' +
                '<h4 class="text-thin">Вы уверены, что хотите удалить отчет?</h4>' +
                'Данные удалены полностью без возможности восстановления' +
                '</div></div>',
                title: "Удаление отчета",
                buttons: {
                    success: {
                        label: "Да, удалить отчет",
                        className: "btn-danger",
                        callback: function () {
                            var objects = {};
                            objects.postUrl = '/deleteRequest/advstat/' + ticket.id;
                            ajaxPost({}, objects, 'block');
                            $('#advstat-table').bootstrapTable('refresh');
                        }
                    },
                    cancel: {
                        label: "Нет, отменить удаление",
                        className: ""
                    }
                }
            });
        }).on('click', '.btn-advstat-subscribe', function () {

            var ticket = {};
            var objects = {};
            ticket.id = $(this).attr('data-ticket');

            objects.button = $('.btn-advstat-subscribe[data-ticket='+ ticket.id +']');
            objects.button.niftyOverlay('show');
            objects.postUrl = '/ajax/subscribeRequest/advstat/' + ticket.id;
            $.ajax({
                type: "POST",
                url: objects.postUrl,
                success: function (res) {
                    console.log("success");
                    console.log(res);
                    checkAjaxPost(res, objects, 'block');
                },
                error: function (a,b,c) {
                    console.log("error");
                    console.dir(a);
                    console.dir(b);
                    console.dir(c);
                },
                complete: function () {
                    console.log("complete");
                    proceed = false;
                    $('#advstat-table').bootstrapTable('refresh');
                    objects.button.niftyOverlay('hide');
                }
            });
        }).on('click', '.btn-advstat-unsubscribe', function () {

            var ticket = {};
            var objects = {};
            ticket.id = $(this).attr('data-ticket');

            objects.button = $('.btn-advstat-unsubscribe[data-ticket='+ ticket.id +']');
            objects.button.niftyOverlay('show');
            objects.postUrl = '/ajax/unsubscribeRequest/advstat/' + ticket.id;
            $.ajax({
                type: "POST",
                url: objects.postUrl,
                success: function (res) {
                    console.log("success");
                    console.log(res);
                    checkAjaxPost(res, objects, 'block');
                },
                error: function (a,b,c) {
                    console.log("error");
                    console.dir(a);
                    console.dir(b);
                    console.dir(c);
                },
                complete: function () {
                    console.log("complete");
                    proceed = false;
                    $('#advstat-table').bootstrapTable('refresh');
                    objects.button.niftyOverlay('hide');
                }
            });
        });
    }

    if ($(".subscribe-btn").length) {
        $('.subscribe-btn').click(function () {

            var objects = {};

            objects.button = $(this);
            objects.button.niftyOverlay('show');
            objects.postUrl = $(this).attr('data-url');
            $.ajax({
                type: "POST",
                url: objects.postUrl,
                data: 'ref=single',
                success: function (res) {
                    console.log("success");
                    console.log(res);
                    checkAjaxPost(res, objects, 'block');
                },
                error: function (a,b,c) {
                    console.log("error");
                    console.dir(a);
                    console.dir(b);
                    console.dir(c);
                },
                complete: function () {
                    console.log("complete");
                    proceed = false;
                }
            });
        })
    }

    $('.cost-table').each(function (e) {
        var table = $(this);
        var id = table.attr('putid');
        table.bootstrapTable(
            {
                queryParams: function custom_query_cost(p) {
                    advstat_id = table.attr('putid');
                    cur = table.attr('putCurrency');
                    filterIdFrom = $("#filterIdFrom").val();
                    filterIdTo = $("#filterIdTo").val();
                    filterCostName = $("#filterCostName").val();
                    filterCostDateFrom = $("#filterDateCostFrom").val();
                    filterCostDateTo = $("#filterDateCostTo").val();
                    filterType = $("#filterType option:selected").val();
                    filterKind = $("#filterKind option:selected").val();
                    filterSumFrom = $("#filterSumFrom").val();
                    filterSumTo = $("#filterSumTo").val();
                    filterCurrency = $("#filterCurrency option:selected").val();
                    filterCourseFrom = $("#filterCourseFrom").val();
                    filterCourseTo = $("#filterCourseTo").val();
                    filterDocNumbFrom = $("#filterDocNumbFrom").val();
                    filterDocNumbTo = $("#filterDocNumbTo").val();
                    return {
                        id: advstat_id,
                        Currency: cur,
                        limit: p.limit,
                        offset: p.offset,
                        sort: p.sort,
                        order: p.order,
                        filterIdFrom: filterIdFrom,
                        filterIdTo: filterIdTo,
                        filterCostName: filterCostName,
                        filterCostDateFrom: filterCostDateFrom,
                        filterCostDateTo: filterCostDateTo,
                        filterType: filterType,
                        filterKind: filterKind,
                        filterSumFrom: filterSumFrom,
                        filterSumTo: filterSumTo,
                        filterCurrency: filterCurrency,
                        filterCourseFrom: filterCourseFrom,
                        filterCourseTo: filterCourseTo,
                        filterDocNumbFrom: filterDocNumbFrom,
                        filterDocNumbTo: filterDocNumbTo,
                    }
                },
                onLoadSuccess: function (data) {
                    if(data.length === 0) {
                        $('#wrap-table-'+table.attr('putCurrency')).hide();
                    } else {
                        $('#wrap-table-'+table.attr('putCurrency')).show();
                        summ = 0;
                        $.each(data, function (i,v) {
                            summ = summ + parseFloat(v.sum);
                        });
                        lastrow = $("<tr><td colspan='10' class='text-right text-bold'>Итого: "+summ+"</td></tr>");
                        table.append(lastrow);
                    }
                },
            }
        ).on('click', '.btn-cost-delete', function () {
            var cost = {};
            cost.id = $(this).attr('data-cost');
            bootbox.dialog({
                message: '<div class="media"><div class="media-body">' +
                '<h4 class="text-thin">Вы уверены, что хотите удалить расход?</h4>' +
                'Данные удалены полностью без возможности восстановления' +
                '</div></div>',
                title: "Удаление расхода",
                buttons: {
                    success: {
                        label: "Да, удалить расход",
                        className: "btn-danger",
                        callback: function () {
                            var objects = {};
                            objects.button = $('button[data-cost='+ cost.id +']');
                            objects.button.niftyOverlay('show');
                            objects.postUrl = '/ajax/deleteRequest/cost/' + cost.id;
                            $.ajax({
                                type: "POST",
                                url: objects.postUrl,
                                success: function (res) {
                                    console.log("success");
                                    console.log(res);
                                    checkAjaxPost(res, objects, 'block');
                                },
                                error: function (a,b,c) {
                                    console.log("error");
                                    console.dir(a);
                                    console.dir(b);
                                    console.dir(c);
                                },
                                complete: function () {
                                    console.log("complete");
                                    proceed = false;
                                    table.bootstrapTable('refresh');
                                    objects.button.niftyOverlay('hide');
                                }
                            });
                        }
                    },
                    cancel: {
                        label: "Нет, отменить удаление",
                        className: ""
                    }
                }
            });
        }).on('click', '.btn-cost-copy', function () {
            var objects = {};
            var cost = {};
            cost.id = $(this).attr('data-cost');
            objects.button = $('button[data-cost='+ cost.id +']');
            objects.button.niftyOverlay('show');
            objects.postUrl = '/copyRequest/cost/' + cost.id;
            ajaxPost({}, objects, 'block');
        });
    });
    
    if ($("#doc-table").length) {
        var id = $("#doc-table").attr('putId');
        $('#doc-table').bootstrapTable(
            {
                queryParams: custom_query_document
            }
        ).on('click', '.btn-doc-delete', function () {
            var doc = {};
            doc.id = $(this).attr('data-doc');
            bootbox.dialog({
                message: '<div class="media"><div class="media-body">' +
                '<h4 class="text-thin">Вы уверены, что хотите удалить документ?</h4>' +
                'Данные удалены полностью без возможности восстановления' +
                '</div></div>',
                title: "Удаление документа",
                buttons: {
                    success: {
                        label: "Да, удалить документ",
                        className: "btn-danger",
                        callback: function () {
                            var objects = {};
                            objects.button = $('button[data-doc='+ doc.id +']');
                            objects.button.niftyOverlay('show');
                            objects.postUrl = '/ajax/deleteRequest/advstatdoc/' + doc.id;
                            $.ajax({
                                type: "POST",
                                url: objects.postUrl,
                                success: function (res) {
                                    console.log("success");
                                    console.log(res);
                                    checkAjaxPost(res, objects, 'block');
                                },
                                error: function (a,b,c) {
                                    console.log("error");
                                    console.dir(a);
                                    console.dir(b);
                                    console.dir(c);
                                },
                                complete: function () {
                                    console.log("complete");
                                    proceed = false;
                                    $('#doc-table').bootstrapTable('refresh');
                                    objects.button.niftyOverlay('hide');
                                }
                            });
                        }
                    },
                    cancel: {
                        label: "Нет, отменить удаление",
                        className: ""
                    }
                }
            });
        });
    }

    if ($("#psreport-table").length) {
        $('#psreport-table').bootstrapTable(
            {
                queryParams: custom_query_psreport
            }
        )
    }

    $('#docs-table-small').on('click', '.btn-doc-delete', function () {
        var doc = {};
        doc.id = $(this).attr('data-doc');
        bootbox.dialog({
            message: '<div class="media"><div class="media-body">' +
            '<h4 class="text-thin">Вы уверены, что хотите удалить документ?</h4>' +
            'Данные удалены полностью без возможности восстановления' +
            '</div></div>',
            title: "Удаление документа",
            buttons: {
                success: {
                    label: "Да, удалить документ",
                    className: "btn-danger",
                    callback: function () {
                        var objects = {};
                        objects.button = $('button[data-doc='+ doc.id +']');
                        objects.button.niftyOverlay('show');
                        objects.postUrl = '/ajax/deleteRequest/advstatdoc/' + doc.id;
                        $.ajax({
                            type: "POST",
                            url: objects.postUrl,
                            success: function (res) {
                                console.log("success");
                                console.log(res);
                                checkAjaxPost(res, objects, 'block');
                            },
                            error: function (a,b,c) {
                                console.log("error");
                                console.dir(a);
                                console.dir(b);
                                console.dir(c);
                            },
                            complete: function () {
                                console.log("complete");
                                proceed = false;
                                objects.button.niftyOverlay('hide');
                                window.location.reload()
                            }
                        });
                    }
                },
                cancel: {
                    label: "Нет, отменить удаление",
                    className: ""
                }
            }
        });
    });
    
    // X-EDITABLE USING FONT AWESOME ICONS
    // =================================================================
    // Require X-editable
    // http://vitalets.github.io/x-editable/
    //
    // Require Font Awesome
    // http://fortawesome.github.io/Font-Awesome/icons/
    // =================================================================
    $.fn.editableform.buttons =
        '<button type="submit" class="btn btn-primary editable-submit">' +
        '<i class="fa fa-fw fa-check"></i>' +
        '</button>' +
        '<button type="button" class="btn btn-default editable-cancel">' +
        '<i class="fa fa-fw fa-times"></i>' +
        '</button>';


    // BOOTSTRAP TABLE - USE NIFTYCHECK TO STYLE THE CHECKBOXES
    // =================================================================
    // Require nifty.js
    // =================================================================
    $('table.add-niftycheck').find('input:checkbox').not('.form-checkbox input:checkbox').wrap('<label class="form-checkbox' +
        ' form-icon"></label>');
    $('table.add-niftycheck').find('.form-checkbox').niftyCheck();

    $(".add-niftycheck").on('post-body.bs.table', function () {
        $(this).find('input:checkbox').not('.form-checkbox input:checkbox').wrap('<label class="form-checkbox form-icon"></label>');
        $(this).find('.form-checkbox').niftyCheck();
    });


    // BOOTSTRAP TABLE - USE NIFTYCHECK TO STYLE THE RADIOS
    // =================================================================
    // Require nifty.js
    // =================================================================
    $(".add-niftyradio").on('post-body.bs.table', function () {
        $(this).find('input:radio').wrap('<label class="form-radio form-icon"></label>');
        $(this).find('.form-radio').niftyCheck();
    });

    if ($("#input_otdel").length) {
        $("#input_otdel").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    url: "/ajax/filterAutocompleteRequest/timecard/main/" + request.term,
                    data: {term: request.term, pole: 'input_otdel'},
                    success: function (data) {
                        if (data != null) {
                            response($.map(data, function (item, key) {
                                return {
                                    label: item,
                                    value: item,
                                    attr: key
                                }
                            }))
                        }
                    },
                    dataType: 'json',
                });
            },
            minLength: 2,
            delay: 1000,
            select: function (a, b) {
                $('#input_otdel_hidden').val(b.item.attr);
            }
        }).data("autocomplete")._renderItem = function (ul, item) {
            return $("<li data-code=\"" + item.value + "\"></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
        };
        $("#input_otdel").keyup(function(){
            if($(this).val().length < 2){
                $('#input_otdel_hidden').val('');
            }
        });
    }
    //year: $('#timecard-table').attr('year'), month: $('#timecard-table').attr('month')
    if ($("#input_profiles").length) {
        $("#input_profiles").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    url: "/ajax/filterAutocompleteRequest/timecard/main/" + request.term,
                    data: {term: request.term, pole: 'input_profiles'},
                    success: function (data) {
                        if (data != null) {
                            response($.map(data, function (item, key) {
                                return {
                                    label: item,
                                    value: item,
                                    attr: key
                                }
                            }))
                        }
                    },
                    dataType: 'json',
                });
            },
            minLength: 2,
            delay: 1000,
            select: function (a, b) {
                $('#input_profiles_hidden').val(b.item.attr);
            }
        }).data("autocomplete")._renderItem = function (ul, item) {
            return $("<li data-code=\"" + item.value + "\"></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
        };
        $("#input_profiles").keyup(function(){
            if($(this).val().length < 2){
                $('#input_profiles_hidden').val('');
            }
        });
    }

    if ($("#input_persnum").length) {
        $("#input_persnum").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    url: "/ajax/filterAutocompleteRequest/timecard/main/" + request.term,
                    data: {term: request.term, pole: 'input_persnum'},
                    success: function (data) {
                        if (data != null) {
                            response($.map(data, function (item, key) {
                                return {
                                    label: item,
                                    value: item,
                                    attr: key
                                }
                            }))
                        }
                    },
                    dataType: 'json',
                });
            },
            minLength: 2,
            delay: 1000,
            select: function (a, b) {
                $('#input_persnum_hidden').val(b.item.attr);
            }
        }).data("autocomplete")._renderItem = function (ul, item) {
            return $("<li data-code=\"" + item.value + "\"></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
        };
        $("#input_persnum").keyup(function(){
            if($(this).val().length < 2){
                $('#input_persnum_hidden').val('');
            }
        });
    }

    if ($("#input_positions").length) {
        $("#input_positions").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    url: "/ajax/filterAutocompleteRequest/timecard/main/" + request.term,
                    data: {term: request.term, pole: 'input_positions'},
                    success: function (data) {
                        if (data != null) {
                            response($.map(data, function (item, key) {
                                return {
                                    label: item,
                                    value: item,
                                    attr: key
                                }
                            }))
                        }
                    },
                    dataType: 'json',
                });
            },
            minLength: 2,
            delay: 1000,
            select: function (a, b) {
                $('#input_positions_hidden').val(b.item.attr);
            }
        }).data("autocomplete")._renderItem = function (ul, item) {
            return $("<li data-code=\"" + item.value + "\"></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
        };
        $("#input_positions").keyup(function(){
            if($(this).val().length < 2){
                $('#input_positions_hidden').val('');
            }
        });
    }
    
    if ($("#form-HrSpecialist-create-hrSpecialist").length) {
        $("#form-HrSpecialist-create-hrSpecialist").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    url: "/ajax/hrSpecialistRequest/timecard/hrmanager/" + request.term,
                    data: {term: request.term},
                    success: function (data) {
                        if (data != null) {
                            response($.map(data, function (item, key) {
                                return {
                                    label: item,
                                    value: item,
                                    attr: key
                                }
                            }))
                        }
                    },
                    dataType: 'json',
                });
            },
            minLength: 2,
            delay: 1000,
            select: function (a, b) {
                $('#form-HrSpecialist-create-hrSpecialistCode').val(b.item.attr);
            }
        }).data("autocomplete")._renderItem = function (ul, item) {
            return $("<li data-code=\"" + item.value + "\"></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
        };
        $("#form-HrSpecialist-create-hrSpecialist").keyup(function(){
         if($(this).val().length < 2){
             $('#form-HrSpecialist-create-hrSpecialistCode').val('');
         }
        });
    }

    if ($("#form-HrBusinesspartner-create-hrBusinesspartner").length) {
        $("#form-HrBusinesspartner-create-hrBusinesspartner").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    url: "/ajax/hrBusinesspartnerRequest/timecard/hrmanager/" + request.term,
                    data: {term: request.term},
                    success: function (data) {
                        if (data != null) {
                            response($.map(data, function (item, key) {
                                return {
                                    label: item,
                                    value: item,
                                    attr: key
                                }
                            }))
                        }
                    },
                    dataType: 'json',
                });
            },
            minLength: 2,
            delay: 1000,
            select: function (a, b) {
                $('#form-HrBusinesspartner-create-hrBusinesspartnerCode').val(b.item.attr);
            }
        }).data("autocomplete")._renderItem = function (ul, item) {
            return $("<li data-code=\"" + item.value + "\"></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
        };
        $("#form-HrBusinesspartner-create-hrBusinesspartner").keyup(function(){
            if($(this).val().length < 2){
                $('#form-HrBusinesspartner-create-hrBusinesspartnerCode').val('');
            }
        });
    }

});

// FORMAT COLUMN
// Use "data-formatter" on HTML to format the display of bootstrap table column.
// =================================================================

function bitrixFormatter(value, row, index) {

}


// Sample format for Invoice Column.
// =================================================================
function invoiceFormatter(value, row) {
    return '<a href="#" class="btn-link" > Order #' + value + '</a>';
}


// Sample Format for User Name Column.
// =================================================================
function nameFormatter(value, row) {
    return '<a href="#" class="btn-link" > ' + value + '</a>';
}


// Sample Format for Order Date Column.
// =================================================================
function dateFormatter(value, row) {
    var icon = row.id % 2 === 0 ? 'fa-star' : 'fa-user';
    return '<span class="text-muted"><i class="fa fa-clock-o"></i> ' + value + '</span>';
}


// Sample Format for Order Status Column.
// =================================================================
function statusFormatter(value, row) {
    var labelColor;
    if (value == "Paid") {
        labelColor = "success";
    } else if (value == "Unpaid") {
        labelColor = "warning";
    } else if (value == "Shipped") {
        labelColor = "info";
    } else if (value == "Refunded") {
        labelColor = "danger";
    }
    var icon = row.id % 2 === 0 ? 'fa-star' : 'fa-user';
    return '<div class="label label-table label-' + labelColor + '"> ' + value + '</div>';
}


// Sample Format for Tracking Number Column.
// =================================================================
function trackFormatter(value, row) {
    if (value) return '<i class="fa fa-plane"></i> ' + value;
}


// Sort Price Column
// =================================================================
function priceSorter(a, b) {
    a = +a.substring(1); // remove $
    b = +b.substring(1);
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
}

// Sort advStat status Column
// =================================================================
if ($("#advstat-status-select").length) {
    $("#advstat-status-select").change(function () {
        $('#advstat-table').bootstrapTable('refresh');
    });
}
if ($("#advstat-role-select").length) {
    $("#advstat-role-select").change(function () {
        $('#advstat-table').bootstrapTable('refresh');
    });
}
if ($("#allFilter").length) {
    $("#allFilter").click(function () {
        $('#advstat-table').bootstrapTable('refresh');
    });
}
if ($("#clearFilter").length) {
    $("#clearFilter").click(function () {
        //$('#demo-panel-collapse-default form')[0].reset();
        $('#demo-panel-collapse-default form select').val("");
        $('#demo-panel-collapse-default form input').val("");
        $('.selectpicker').selectpicker('render');
        $('#advstat-table').bootstrapTable('refresh');
    });
}
if ($("#allFilterCost").length) {
    $("#allFilterCost").click(function () {
        if ($("#filterCurrency option:selected").val()>0) {
            currencyId = $("#filterCurrency option:selected").val();
            $('.cost-table').each(function (e) {
                if($(this).attr('putCurrency') != currencyId) {
                    $('#wrap-table-'+$(this).attr('putCurrency')).hide();
                } else {
                    $('#wrap-table-'+currencyId).show();
                    $(this).bootstrapTable('refresh');
                }
            });
        } else {
            $('.cost-table').each(function (e) {
                $(this).bootstrapTable('refresh');
            });
        }
    });
}
if ($("#allFilterCostCancel").length) {
    $("#allFilterCostCancel").click(function () {
        $('#cost-filter-panel input[type=text]').val('');
        $('#cost-filter-panel select option').removeAttr("selected");
        $('.selectpicker').selectpicker('render');
        $('.cost-table').each(function (e) {
            $(this).bootstrapTable('refresh');
        });
    });
}

if ($("#allFilterDoc").length) {
    $("#allFilterDoc").click(function () {
        $('#doc-table').bootstrapTable('refresh');
    });
}
if ($("#allClearFilterDoc").length) {
    $("#allClearFilterDoc").click(function () {
        $('#filter-advstatdoc input[type=text]').val('');
        $('#filter-advstatdoc select option').removeAttr("selected");
        $('.selectpicker').selectpicker('render');
        $('#doc-table').bootstrapTable('refresh');
    });
}

if ($("#allFilterReference").length) {
    $("#allFilterReference").click(function () {
        $("#allFilterReference").niftyOverlay('show');
        $('#reference-table').bootstrapTable('refresh');
        var filterTitle = $('#filter-title');
        var filterTitleText = filterTitle.attr('data-text');
        filterTitle.text(filterTitleText);
    });
}
if ($("#clearFilterReference").length) {
    $("#clearFilterReference").click(function () {
        $('#filter-reference input[type=text]').val('');
        $('#filter-reference select option').removeAttr("selected");
        $('.selectpicker').selectpicker('render');
        var filterTitle = $('#filter-title');
        var filterTitleText = filterTitle.attr('data-text');
        filterTitle.text(filterTitleText);
        $('#reference-table').bootstrapTable('refresh');
    });
}

if ($("#widgetsAgreement").length) {
    $("#widgetsAgreement").on('click',function () {
        $('#filter-reference input[type=text]').val('');
        $('#filter-reference select option').removeAttr("selected");
        $('.selectpicker').selectpicker('render');
        var select = $("#filterReferenceStatus");
        var filterTitle = $('#filter-title');
        var filterTitleText = filterTitle.attr('data-text');
        var windgetLang = $('#widgetsAgreementText').text();
        var new_text = filterTitleText + ' (' + Language.referenceStatus + ': ' + windgetLang + ')';
        select.val(1);
        select.selectpicker('refresh');
        filterTitle.text(new_text);
        var ForOverlay = $("#allFilterReference").attr('data-target');
        if (!$(ForOverlay).hasClass("panel-overlay-wrap")) {
            $("#allFilterReference").niftyOverlay('show');
        }
        $('#reference-table').bootstrapTable('refresh');
    });
}
if ($("#widgetsInWork").length) {
    $("#widgetsInWork").on('click',function () {
        $('#filter-reference input[type=text]').val('');
        $('#filter-reference select option').removeAttr("selected");
        $('.selectpicker').selectpicker('render');
        var select = $("#filterReferenceStatus");
        var filterTitle = $('#filter-title');
        var filterTitleText = filterTitle.attr('data-text');
        var windgetLang = $('#widgetsInWorkText').text();
        var new_text = filterTitleText + ' (' + Language.referenceStatus + ': ' + windgetLang + ')';
        select.val(2);
        select.selectpicker('refresh');
        filterTitle.text(new_text);
        var ForOverlay = $("#allFilterReference").attr('data-target');
        if (!$(ForOverlay).hasClass("panel-overlay-wrap")) {
            $("#allFilterReference").niftyOverlay('show');
        }
        $('#reference-table').bootstrapTable('refresh');
    });
}
if ($("#widgetsResults").length) {
    $("#widgetsResults").on('click', function () {
        $('#filter-reference input[type=text]').val('');
        $('#filter-reference select option').removeAttr("selected");
        $('.selectpicker').selectpicker('render');
        var select = $("#filterReferenceStatus");
        var filterTitle = $('#filter-title');
        var filterTitleText = filterTitle.attr('data-text');
        var windgetLang = $('#widgetsResultsText').text();
        var new_text = filterTitleText + ' (' + Language.referenceStatus + ': ' + windgetLang + ')';
        select.val(5);
        select.selectpicker('refresh');
        filterTitle.text(new_text);
        var ForOverlay = $("#allFilterReference").attr('data-target');
        if (!$(ForOverlay).hasClass("panel-overlay-wrap")) {
            $("#allFilterReference").niftyOverlay('show');
        }
        $('#reference-table').bootstrapTable('refresh');
    });
}
if ($("#widgetsReferenceIn").length) {
    $("#widgetsReferenceIn").on('click', function () {
        $('#filter-reference input[type=text]').val('');
        $('#filter-reference select option').removeAttr("selected");
        $('.selectpicker').selectpicker('render');
        var filterCreateFrom = $("#filterCreateFrom");
        var filterCreateTo = $("#filterCreateTo");
        var filterTitle = $('#filter-title');
        var filterTitleText = filterTitle.attr('data-text');
        var new_text = filterTitleText + ' (' + Language.referenceCreatePeriod + ': ' + monthLang + ')';
        filterCreateFrom.val(RfMonthDays.first);
        filterCreateTo.val(RfMonthDays.last);
        filterTitle.text(new_text);
        var ForOverlay = $("#allFilterReference").attr('data-target');
        if (!$(ForOverlay).hasClass("panel-overlay-wrap")) {
            $("#allFilterReference").niftyOverlay('show');
        }
        $('#reference-table').bootstrapTable('refresh');
    });
}
if ($("#widgetsReferenceItogo").length) {
    $("#widgetsReferenceItogo").on('click', function () {
        $('#filter-reference input[type=text]').val('');
        $('#filter-reference select option').removeAttr("selected");
        $('.selectpicker').selectpicker('render');
        var filterTitle = $('#filter-title');
        var filterTitleText = filterTitle.attr('data-text');
        var new_text = filterTitleText;
        filterTitle.text(new_text);
        var ForOverlay = $("#allFilterReference").attr('data-target');
        if (!$(ForOverlay).hasClass("panel-overlay-wrap")) {
            $("#allFilterReference").niftyOverlay('show');
        }
        $('#reference-table').bootstrapTable('refresh');
    });
}




function custom_queryReference(p) {
    if(p) { // Если вызвано из BootstrapTable
        referencelimit = p.limit;
        referenceoffset = p.offset;
        referencesort = p.sort;
        referenceorder = p.order;
        referencesearch = p.search;
        referenceTableParams = p; // Для функции печати reference.js
    } else { // Из функции печати
        referencelimit = false;
        referenceoffset = false;
        referencesort = false;
        referenceorder = false;
        referencesearch = false;
    }
    requestFor = $("#reference-table").attr('data-for');
    filterIdFrom = $("#filterIdFrom").val();
    filterIdTo = $("#filterIdTo").val();
    filterType = $("#filterReferenceType option:selected").val();
    filterStatus = $("#filterReferenceStatus option:selected").val();
    filterOwner = $("#filterReferenceOwner option:selected").val();
    filterAuthor = $("#filterReferenceAuthor option:selected").val();
    filterDelivery = $("#filterReferenceDelivery option:selected").val();
    filterCreatedFrom = $("#filterCreateFrom").val();
    filterCreatedTo = $("#filterCreateTo").val();
    return {
        limit: referencelimit,
        offset: referenceoffset,
        sort: referencesort,
        order: referenceorder,
        search: referencesearch,
        requestFor: requestFor,
        filterIdFrom: filterIdFrom,
        filterIdTo: filterIdTo,
        filterType: filterType,
        filterStatus: filterStatus,
        filterOwner: filterOwner,
        filterAuthor: filterAuthor,
        filterDelivery: filterDelivery,
        filterCreatedFrom: filterCreatedFrom,
        filterCreatedTo: filterCreatedTo,
    }
}

function custom_query(p) {
    fastStatus = $("#advstat-status-select option:selected").val();
    roleStatus = $("#advstat-role-select option:selected").val();
    filterIdFrom = $("#filterIdFrom").val();
    filterIdTo = $("#filterIdTo").val();
    filterReportName = $("#filterReportName").val();
    filterEmployee = $("#filterEmployee option:selected").val();
    filterAuthor = $("#filterAuthor option:selected").val();
    filterUl = $("#filterUl option:selected").val();
    filterCost = $("#filterCost option:selected").val();
    filterEditFrom = $("#filterEditFrom").val();
    filterEditTo = $("#filterEditTo").val();
    filterObserver = $("#filterObserver option:selected").val();
    filterStatus = $("#filterStatus option:selected").val();
    filterCreatedFrom = $("#filterCreatedFrom").val();
    filterCreatedTo = $("#filterCreatedTo").val();
    return {
        limit: p.limit,
        offset: p.offset,
        sort: p.sort,
        order: p.order,
        search: p.search,
        fastStatus: fastStatus,
        roleStatus: roleStatus,
        filterIdFrom: filterIdFrom,
        filterIdTo: filterIdTo,
        filterReportName: filterReportName,
        filterEmployee: filterEmployee,
        filterAuthor: filterAuthor,
        filterUl: filterUl,
        filterCost: filterCost,
        filterEditFrom: filterEditFrom,
        filterEditTo: filterEditTo,
        filterObserver: filterObserver,
        filterStatus: filterStatus,
        filterCreatedFrom: filterCreatedFrom,
        filterCreatedTo: filterCreatedTo,
    }
}

function custom_query_document(p) {
    id = $("#dost-table").attr('putId');
    filterIdFrom = $("#filterIdFrom").val();
    filterIdTo = $("#filterIdTo").val();
    filterDocName = $("#filterDocName").val();
    filterFileName = $("#filterFileName").val();
    filterDateDocFrom = $("#filterDateDocFrom").val();
    filterDateDocTo = $("#filterDateDocTo").val();
    return {
        id: id,
        limit: p.limit,
        offset: p.offset,
        sort: p.sort,
        order: p.order,
        filterIdFrom: filterIdFrom,
        filterIdTo: filterIdTo,
        filterDocName: filterDocName,
        filterFileName: filterFileName,
        filterDateDocFrom: filterDateDocFrom,
        filterDateDocTo: filterDateDocTo,
    }
}

function custom_query_psreport(p) {
    id = $("#psreport-table").attr('putId');
    return {
        id: id,
        limit: p.limit,
        offset: p.offset,
        sort: p.sort,
        order: p.order,
        search: p.search
    }
}

// Sort Master Column
// =================================================================
if ($("#master-filter-structure").length) {
    $("#master-filter-structure").change(function () {
        $('#master-employees-table').bootstrapTable('refresh');
    });
}

if ($("#master-filter-employee").length) {
    $("#master-filter-employee").change(function () {
        $('#master-employees-table').bootstrapTable('refresh');
    });
}

function custom_query_master(p) {
    id = $("#master-employees-table").attr('putid');
    filterStructure = $("#master-filter-structure option:selected").val();
    filterEmployee = $("#master-filter-employee option:selected").val();
    p.id = id;
    p.limit = p.limit;
    p.offset = p.offset;
    p.sort = p.sort;
    p.order = p.order;
    p.search = p.search;
    p.filterStructure = filterStructure;
    p.filterEmployee = filterEmployee;
    return p
}