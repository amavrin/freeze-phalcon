$(document).ready(function(){

    var selContractorUserM1 = $("#filterContractorUserM1");
    var selContractorUserM2 = $("#filterContractorUserM2");
    var selContractorUserM3 = $("#filterContractorUserM3");
    var selContractorStockM3 = $("#filterContractorStockM3");
    var selContractorUserM4 = $("#filterContractorUserM4");
    var selContractorStockM4 = $("#filterContractorStockM4");

    if (selContractorUserM3.length) {
        selContractorUserM3.change(function () {
            selContractorStockM3.parent().show();
            var objects = {};
            var postData = {'contractor': $(this).val()};
            objects.postUrl = "/ajax/stockRequest/index";
            $.ajax({
                type: "POST",
                url: objects.postUrl,
                data: postData,
                success: function (res) {
                    if(res) {
                        var SelId = '#filterContractorStockM3';
                        selectResult(SelId, res.data.result);
                    }
                    console.log("success");
                    console.log(res);
                },
                error: function (a, b, c) {
                    console.log("error");
                    if (objects.button) {
                        objects.button.niftyOverlay('hide');
                    }
                },
                complete: function () {

                    console.log("complete");
                    if (objects.button) {
                        objects.button.niftyOverlay('hide');
                    }
                    proceed = false;
                }
            });
        });
    }

    if (selContractorUserM4.length) {
        selContractorUserM4.change(function () {
            selContractorStockM4.parent().show();
            var objects = {};
            var postData = {'contractor': $(this).val()};
            objects.postUrl = "/ajax/stockRequest/index";
            $.ajax({
                type: "POST",
                url: objects.postUrl,
                data: postData,
                success: function (res) {
                    if(res) {
                        var SelId = '#filterContractorStockM4';
                        selectResult(SelId, res.data.result);
                    }
                    console.log("success");
                    console.log(res);
                },
                error: function (a, b, c) {
                    console.log("error");
                    if (objects.button) {
                        objects.button.niftyOverlay('hide');
                    }
                },
                complete: function () {

                    console.log("complete");
                    if (objects.button) {
                        objects.button.niftyOverlay('hide');
                    }
                    proceed = false;
                }
            });
        });
    }

    $('form[meta-ajax="true"]').submit(function (e) {
        e.preventDefault();
        if (!proceed) {
            proceed = true;
            var objects = {};
            objects.form = $(this);
            objects.postUrl = "/ajax/indexRequest/index";
            objects.button = $(this).find(':submit');
            objects.button.niftyOverlay('show');
            var formData = new FormData($(this)[0]);
            $.ajax({
                type: "POST",
                url: objects.postUrl,
                data: formData,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function (res) {
                    if(res) {
                        var UlId = objects.button.attr('data-ul');
                        viewResult(UlId, res.data.result);
                    }
                    console.log("success");
                    console.log(res);
                },
                error: function (a, b, c) {
                    console.log("error");
                    if (objects.button) {
                        objects.button.niftyOverlay('hide');
                    }
                },
                complete: function () {

                    console.log("complete");
                    if (objects.button) {
                        objects.button.niftyOverlay('hide');
                    }
                    proceed = false;
                }
            });
        }
        return false;
    });

    function viewResult(UlId, data){
        var Ul = $(UlId);
        var lis = Ul.find('li');
        lis.remove();
        var row = '';
        $.each(data,function(index,value){
            row += '<li class="list-group-item">';
            $.each(value,function(Vindex,Vvalue){
                row += ' ' + Vvalue + ' |';
            });
            row += '</li>';
        });
        Ul.prepend(row);
    }

    function selectResult(SelId, data){
        var Sel = $(SelId);
        var options = Sel.find('option');
        options.remove();
        var row = '';
        $.each(data,function(index,value){
            row += '<option value="'+value.id+'">'+value.name+'</li>';
        });
        Sel.prepend(row);
        Sel.selectpicker('refresh');

    }
});