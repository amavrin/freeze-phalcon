<?php
namespace Components;

use Phalcon\Exception;

class ConvertExcel extends \Phalcon\Di\Injectable
{

    public function __construct()
    {

    }

    public function XLSXtoArray($inputFile=false, $dir_tmp=false, $all_firstrow_skip=false, $skip_rows=[], $skip_colls=[], $empty_cols = [])
    {
        if($inputFile===false && $dir_tmp===false)
        {
            $this->log->log(__CLASS__.__METHOD__." Не указан полный путь до файла-источника и/или tmp директория");
            throw new Exception('file path or tmp folder path not specified.');
        }

        if (!$zip = new \ZipArchive())
        {
            $this->log->log(__CLASS__.__METHOD__." PHP ZIP расширение выключено");
            throw new Exception('php zip class not specified.');
        }
        $zip->open($inputFile);
        $zip->extractTo($dir_tmp);
        /*Проверка на Unzip*/
        if (!file_exists($dir_tmp . '/xl/sharedStrings.xml'))
        {
            $this->log->log(__CLASS__.__METHOD__." sharedStrings.xml не найден, ошибка в распаковке.");
            throw new Exception('sharedStrings.xml not found, error unzip.');
        }
        /*Получаем все переменные из всех листов*/
        $strings = simplexml_load_file($dir_tmp . '/xl/sharedStrings.xml');
        $arr = [];
        $i=0;
        /*Получаем все XML листов из tmp папки*/
        foreach (glob($dir_tmp.'/xl/worksheets/*.xml') as $sheet) {
            $i++;
            if (!file_exists($sheet))
            {
                $this->log->log(__CLASS__.__METHOD__." Файл листа (".$sheet.") не найден");
                throw new Exception($sheet.' not found.');
            }
            /*Читаем XML листа из tmp папки*/
            $sheet = simplexml_load_file($sheet);
            $xlrows = $sheet->sheetData->row;
            $arr_sheet = [];
            $row=0;
            foreach ($xlrows as $xlrow) {
                $arr_xlrow = [];
                $row++;
                /*Пропускаем указанные строки на конкрентном листе*/
                if(is_array($skip_rows) && !empty($skip_rows) && in_array($row, $skip_rows[$i])) {
                    continue;
                }
                /*Пропускаем первую строку на всех листах*/
                if($all_firstrow_skip !== false && $row===1) {
                    continue;
                }
                $coll=0;
                if (is_array($empty_cols) && !empty($empty_cols)) {
                    $skip = false;
                    foreach ($empty_cols as $empty_col) {
                        $el = $strings->si[(int)$xlrow->c[$empty_col]->v];
                        if (empty($el->t)) {
                            $skip = true;
                        }
                    }
                    if ($skip) {
                        continue;
                    }
                }
                foreach ($xlrow->c as $cell) {
                    $coll++;
                    /*Пропускаем указанные ячейки на конкрентном листе*/
                    if(is_array($skip_colls) && !empty($skip_colls) && in_array($coll, $skip_colls[$i])) {
                        continue;
                    }
                    $v = (string)$cell->v;
                    if (isset($cell['t']) && $cell['t'] == 's') {
                        $s = array();
                        $si = $strings->si[(int)$v];
                        $si->registerXPathNamespace('n', 'http://schemas.openxmlformats.org/spreadsheetml/2006/main');
                        foreach ($si->xpath('.//n:t') as $t) {
                            $s[] = (string)$t;
                        }
                        $v = implode($s);
                    }
                    $arr_xlrow['coll_'.$coll] = $v;
                }
                $arr_sheet['row_'.$row] = $arr_xlrow;
            }
            $arr['sheet'.$i] = $arr_sheet;
        }

        return $arr;

    }

}