<?php

namespace Components;

use \Phalcon\Exception,
    \Phalcon\Http\Request\File as FileRequest;

class FileSystem extends \Phalcon\Di\Injectable
{
    private $uploadDir;


    public function saveLocalFile($nameFolder, FileRequest $file, $renameFilename='', $timestamp=false)
    {
        if($renameFilename){
            $filename = $renameFilename;
        } else {
            $filename = $this->getTranslateFileName($file->getName(), $timestamp);
        }
        if (!$file->moveTo($this->createFolder($nameFolder).$filename)) {
            $this->log->error(__CLASS__.__METHOD__." Файл не удалось скопировать в папку ".$this->uploadDir);
            throw new Exception('save file fault.');
        }

        return true;
    }

    public function getUploadDir()
    {
        if (!$this->uploadDir) {
            $this->uploadDir = realpath(rtrim($this->config->files->uploadDir, DIRECTORY_SEPARATOR));
            if (!file_exists($this->uploadDir)) {
                $this->log->error(__CLASS__.__METHOD__." Невозможно получить доступ к папке:".$this->uploadDir);
                throw new Exception('Upload Folder fault, check folder availability');
            }
        }

        return $this->uploadDir;
    }

    public function createFolder($name)
    {
        $filePath = $this->getUploadDir().DIRECTORY_SEPARATOR.$name;
        if (!file_exists($filePath)) {
            if (!mkdir($filePath, 0755, true)) {
                $this->log->error(__CLASS__.__METHOD__." Не удалось создать папку в ".$filePath."");
                throw new Exception('Can\'t create folder:'.$name);
            }
        }

        return $filePath.DIRECTORY_SEPARATOR;
    }

    public function getTranslateFileName($name, $timestamp = false) {
        if($name) {
            $toReplace = array('?',':','*','/','\\','>','<','|','"','\'',' ');
            $name = str_replace($toReplace, '_', $name);
            $format_file = explode('.', $name);
            $format_file = $format_file[count($format_file)-1];
            $file_name = substr ($name, 0, strrpos($name, '.'));
            $trans = array(
                "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "e", "ж" => "zh", "з" => "z", "и" => "i", "й" => "y", "к" => "k",
                "л" => "l", "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "kh", "ц" => "ts",
                "ч" => "ch", "ш" => "sh", "щ" => "shch", "ы" => "y", "э" => "e", "ю" => "yu", "я" => "ya", "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D",
                "Е" => "E", "Ё" => "E", "Ж" => "Zh", "З" => "Z", "И" => "I", "Й" => "Y", "К" => "K", "Л" => "L", "М" => "M", "Н" => "N", "О" => "O", "П" => "P", "Р" => "R",
                "С" => "S", "Т" => "T", "У" => "U", "Ф" => "F", "Х" => "Kh", "Ц" => "Ts", "Ч" => "Ch", "Ш" => "Sh", "Щ" => "Shch", "Ы" => "Y", "Э" => "E", "Ю" => "Yu",
                "Я" => "Ya", "Ъ" => "J", "ъ" => "j", "ь" => "_", "Ь" => "_");
            $file_name = strtr($file_name, $trans);
            if($timestamp !== false) {
                return $name = $file_name.time().".".$format_file;
            }
            return $name = $file_name.".".$format_file;
        }
    }

}