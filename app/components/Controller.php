<?php
namespace Components;


class Controller extends \Phalcon\Mvc\Controller
{
    
    protected function returnData()
    {
        if ($this->request->isAjax()) {
            $response = self::sendAjax();
        }else{
            $response = self::sendPost();
        }
        return $response;
    }

    private function sendPost()
    {
        return $this->post;
    }

    private function sendAjax()
    {
        $this->response->setStatusCode(200, "OK");
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent(json_encode($this->ajax));
        return $this->response;
    }
}