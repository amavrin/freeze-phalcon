<?php
namespace Components;

use \Phalcon\Events\Manager as EventsManager,
    \Phalcon\Mvc\View\Engine\Volt,
    \Phalcon\Assets\Manager as AssetsManager,
    \Phalcon\Assets\Filters\Cssmin,
    \Phalcon\Assets\Filters\Jsmin,
    \Phalcon\Mvc\View;



class Viewer extends \Phalcon\Di\Injectable
{
    public $mainView = 'default';
    public $layout = 'default';
    public $assets;
    public $view;

    public function getAssets()
    {
        if($this->assets) {
            return $this->assets;
        }
        $src = $this->config->application->srcDir;
        $this->assets = new AssetsManager([
            'output'        => realpath($src),
            'compileAlways' => true,
            'stat'          => true
        ]);

        /*
         * CSS
         */
        $this->assets->collection('cssHeader')
            ->setTargetPath("public/public/assets/css/header.css")
            ->setTargetUri("/public/public/assets/css/header.css")
            ->addCss("public/public/css/bootstrap.css")
            ->addCss("public/public/css/nifty.css")
            ->addCss("public/public/css/common/nifty.css")
            ->addCss("public/public/css/demo/nifty-demo.min.css")
            ->addCss("public/public/css/main.css")
            ->addCss("public/public/plugins/font-awesome/css/font-awesome.css")
            ->addCss("public/public/plugins/bootstrap-select/bootstrap-select.css");

        /*
         * JS
         */
        $this->assets->collection('jsHeader')
            ->setTargetPath("public/public/js/header.js")
            ->setTargetUri("/public/public/assets/js/header.js")
            ->addInlineJs("var proceed = false;");

        $this->assets->collection("jsFooter")
            ->setTargetPath("public/public/assets/js/footer.js")
            ->setTargetUri("/public/public/assets/js/footer.js")
            ->addJs("public/public/js/jquery-2.1.1.js")
            ->addJs("public/public/js/bootstrap.js")
            ->addJs("public/public/js/nifty.js")
            ->addJs("public/public/js/common/nifty.js")
            ->addJs("public/public/plugins/bootstrap-select/bootstrap-select.js");

        return $this->assets;
    }


    public function addPlugin($name){
        $src = $this->config->application->srcDir;
        switch($name){
            case 'method':
                $this->assets
                    ->collection("jsFooter")
                    ->addJs("public/public/js/method.js");
                break;
        }
    }

    public function getView(){

        if($this->view) {
            return $this->view;
        }
        $view = $this->view = new View();
        $view->setViewsDir($this->config->application->viewsDir);
        $view->setPartialsDir($this->config->application->partialsDir);
        //Установка шаблона по умолчанию
        $view->setMainView($this->mainView);
        $view->setLayout($this->layout);
        $view->registerEngines(array(
            '.volt' => function ($view, $this) {
                $volt = new Volt($view, $this);
                $volt->setOptions(array(
                    'compiledPath' => $this->config->application->cacheDir . '/volt/',
                    'compiledSeparator' => '_'
                ));
                return $volt;
            }
        ));

        return $this->view;
    }
}