<?php
namespace Controllers;

use Controllers\ControllerBase;

class IndexController extends ControllerBase
{

	public function indexAction()
    {
        // Пользователи-контрагенты
        $contractor =\Models\Contractor::find();
        $ContractorUser = [];
        foreach($contractor as $el) {
            $ContractorUser[$el->user_id] = $el->User->getFullName('short');
        }
        // Контрагенты - склады
        $ContractorStock = [];
        // Селект Пользователи-Контрагенты (получить пользователей)
        $filterContractorUserM1 = $this->tag->selectStatic(
            [
                "filterContractorUserM1",
                $ContractorUser,
                "useEmpty" => true,
                "emptyText" => "Выберите значение...",
                "class" => 'selectpicker',
                "data-width" => '100%',
                "data-live-search" => 'true',
                "name" => 'contractor'
            ]
        );
        // Селект Пользователи-Контрагенты (получить склады)
        $filterContractorUserM2 = $this->tag->selectStatic(
            [
                "filterContractorUserM2",
                $ContractorUser,
                "useEmpty" => true,
                "emptyText" => "Выберите значение...",
                "class" => 'selectpicker',
                "data-width" => '100%',
                "data-live-search" => 'true',
                "name" => 'contractor'
            ]
        );
        // Селект Пользователи-Контрагенты (получить продукты)
        $filterContractorUserM3 = $this->tag->selectStatic(
            [
                "filterContractorUserM3",
                $ContractorUser,
                "useEmpty" => true,
                "emptyText" => "Выберите значение...",
                "class" => 'selectpicker',
                "data-width" => '100%',
                "data-live-search" => 'true',
                "name" => 'contractor'
            ]
        );
        // Селект Контрагент-Склады (получить продукты)
        $filterContractorStockM3 = $this->tag->selectStatic(
            [
                "filterContractorStockM3",
                $ContractorStock,
                "useEmpty" => true,
                "emptyText" => "Выберите значение...",
                "class" => 'selectpicker',
                "data-width" => '100%',
                "data-live-search" => 'true',
                "name" => 'stock'
            ]
        );
        // Селект Пользователи-Контрагенты (импорт продуктов)
        $filterContractorUserM4 = $this->tag->selectStatic(
            [
                "filterContractorUserM4",
                $ContractorUser,
                "useEmpty" => true,
                "emptyText" => "Выберите значение...",
                "class" => 'selectpicker',
                "data-width" => '100%',
                "data-live-search" => 'true',
                "name" => 'contractor'
            ]
        );
        // Селект Контрагент-Склады (импорт продуктов)
        $filterContractorStockM4 = $this->tag->selectStatic(
            [
                "filterContractorStockM4",
                $ContractorStock,
                "useEmpty" => true,
                "emptyText" => "Выберите значение...",
                "class" => 'selectpicker',
                "data-width" => '100%',
                "data-live-search" => 'true',
                "name" => 'stock'
            ]
        );
        $this->view->setVar('filterContractorUserM1', $filterContractorUserM1);
        $this->view->setVar('filterContractorUserM2', $filterContractorUserM2);
        $this->view->setVar('filterContractorUserM3', $filterContractorUserM3);
        $this->view->setVar('filterContractorStockM3', $filterContractorStockM3);
        $this->view->setVar('filterContractorUserM4', $filterContractorUserM4);
        $this->view->setVar('filterContractorStockM4', $filterContractorStockM4);
        $this->viewer->addPlugin('table');
        $this->viewer->addPlugin('method');
	}


    public function indexRequestAction()
    {
        $method = $this->request->getPost('method', 'string');
        $ContractorUserId = $this->request->getPost('contractor', 'string');
        $ContractorStoreId = $this->request->getPost('stock', 'string');
;
        if ($this->request->isAjax() && $method) {
            switch ($method) {
                case 'method1' :
                    $result = $this->getUsersArr($ContractorUserId);
                    if ($result !== false) {
                        $this->ajax['data']['result'] = $result;
                    }
                    break;
                case 'method2' :
                    $result = $this->getStoresArr($ContractorUserId);
                    if ($result !== false) {
                        $this->ajax['data']['result'] = $result;
                    }
                    break;
                case 'method3' :
                    $result = $this->getProductsArr($ContractorStoreId);
                    if ($result !== false) {
                        $this->ajax['data']['result'] = $result;
                    }
                    break;
                case 'method4' :
                    if ($this->request->hasFiles()) {
                        foreach ($this->request->getUploadedFiles() as $file) {
                            if (!empty($file->getName())) {
                                $filename = $file->getName();
                                $rename_filename = $this->fs->getTranslateFileName($filename, true);
                                $filepath = 'product/'.time();
                                $success_upload = $this->fs->saveLocalFile($filepath, $file, $rename_filename, false);
                                if ($success_upload === true) {
                                    $inputFile = $this->config->files->uploadDir . '/' . $filepath.'/'.$rename_filename;
                                    $dir_tmp = $this->config->files->uploadDir . '/' . $filepath . '/tmp';
                                    $data = $this->excel->XLSXtoArray($inputFile, $dir_tmp, false, '', '');
                                    $result = $this->saveProducts($data, $ContractorStoreId);
                                    if($result !== false) {
                                        $this->ajax['data']['result'] = $result;
                                    }
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                    break;
                default :
                    break;
            }
            return self::returnData();
        }
        return false;
    }

    public function stockRequestAction()
    {
        $ContractorUserId = $this->request->getPost('contractor');
        if($ContractorUserId) {
            $result = $this->getStoresArr($ContractorUserId);
            if ($result !== false) {
                $this->ajax['data']['result'] = $result;
                return self::returnData();
            }
            return false;
        }
        return false;
    }

    private function getUsersArr($user_id=null)
    {
        if($user_id) {
            $contractor = \Models\Contractor::findFirst([
                "user_id = :user_id:",
                "bind" => ["user_id" => $user_id]
            ]);
            if($contractor instanceof \Models\Contractor) {
                $data = [];
                foreach($contractor->RlUser as $user) {
                    if($user instanceof  \Models\User) {
                        $data[] = [
                            'id' => $user->id,
                            'name' => $user->getFullName(),
                            'date_create' => $user->date_create
                        ];
                    }
                }
                return $data;
            }
        }
        return false;
    }

    private function getStoresArr($user_id=null)
    {
        if($user_id) {
            $contractor = \Models\Contractor::findFirst([
                "user_id = :user_id:",
                "bind" => ["user_id" => $user_id]
            ]);
            if($contractor instanceof \Models\Contractor) {
                $data = [];
                $stocksModel = \Models\Stock::find([
                    "contractor_id = :user_id:",
                    "bind" => ["user_id" => $contractor->id]
                ]);
                foreach($stocksModel as $stock) {
                    $data[] = [
                        'id' => $stock->id,
                        'name' => $stock->name,
                        'region' => $stock->region,
                        'city' => $stock->city,
                        'phone' => $stock->phone,
                        'email' => $stock->email
                    ];
                }
                return $data;
            }
        }
        return false;
    }

    private function getProductsArr($store_id=null)
    {
        if($store_id) {
            $stocksModel = \Models\Stock::findFirst($store_id);
            if($stocksModel instanceof \Models\Stock) {
                $data = [];
                foreach($stocksModel->RlProduct as $product) {
                    if($product instanceof  \Models\Product) {
                        $data[] = [
                            'id' => $product->id,
                            'name' => $product->name,
                            'artikul' => $product->artikul,
                            'brand' => $product->brand,
                            'date_create' => $product->date_create,
                        ];
                    }
                }
                return $data;
            }
        }
        return false;
    }

    private function saveProducts($data=null, $ContractorStoreId)
    {
        if(is_array($data) && !empty($data) && $ContractorStoreId) {
            $dataResult = [];
            foreach ($data as $sheet => $rows) {
                foreach ($rows as $key => $row) {
                    $product_name = $row['coll_1'];
                    $product_artikul = $row['coll_2'];
                    $product_brand = $row['coll_3'];
                    $product_quantity = $row['coll_4'];
                    // TODO здесь должен быть валидатор входящих данных
                    if($product_name && $product_artikul && $product_brand && $product_quantity){
                        $newProduct = new \Models\Product();
                        $newProduct->name = $product_name;
                        $newProduct->artikul = $product_artikul;
                        $newProduct->brand = $product_brand;
                        $dateObj = new \DateTime('now');
                        $newProduct->date_create = $dateObj->format('Y-m-d');
                        if($newProduct->save()) {
                            $rlProductStock = new \Models\RlProductStock();
                            $rlProductStock->product_id = $newProduct->id;
                            $rlProductStock->stock_id = $ContractorStoreId;
                            $rlProductStock->quantity = $product_quantity;
                            if($rlProductStock->save()) {
                                $dataResult[] = [
                                    'id' => $newProduct->id,
                                    'name' => $newProduct->name,
                                    'artikul' => $newProduct->artikul,
                                    'brand' => $newProduct->brand,
                                    'date_create' => $newProduct->date_create,
                                    'quantity' => $product_quantity,
                                ];
                            } else {
                                /*$messages = $rlProductStock->getMessages();
                                foreach ($messages as $message) {
                                    echo "Message: ", $message->getMessage();
                                    echo "Field: ", $message->getField();
                                    echo "Type: ", $message->getType();
                                }*/
                                continue;
                            }
                        } else {
                            /*$messages = $newProduct->getMessages();
                            foreach ($messages as $message) {
                                echo "Message: ", $message->getMessage();
                                echo "Field: ", $message->getField();
                                echo "Type: ", $message->getType();
                            }*/
                            continue;
                        }
                    } else {
                        //TODO здесь логер, обработчик кодов ошибок или т.п.
                        continue;
                    }
                }
            }
            return $dataResult;
        }
        return false;
    }

}
