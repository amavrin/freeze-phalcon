<?php
/**
 * Регистрация сервисов
 *
 * @var \Phalcon\Config $config
 */

use \Phalcon\Assets,
    \Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter,
    \Phalcon\Logger\Adapter\File as FileLogger,
    \Phalcon\Logger\Formatter\Line as FormatterLine,
    \Phalcon\Mvc\Url as UrlProvider,
    \Phalcon\Mvc\Model\MetaData\Memory as MetaData,
    \Phalcon\Mvc\Application,
    \Phalcon\Loader,
    \Phalcon\Mvc\View,
    \Phalcon\Security,
    \Phalcon\Mvc\Dispatcher,
    \Phalcon\Events\Manager as EventsManager,
    \Phalcon\Session\Adapter\Files as SessionAdapter;


/** Плагины */

$config = include_once CONFIG_PATH;
$loader = include_once LOADER_PATH;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */

$di = new \Phalcon\DI\FactoryDefault();


$di->setShared('config', $config);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */

$di->setShared('db', function () use ($config) {
    return new DbAdapter($config->database->toArray());
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', new MetaData());

/**
 * Запускает сессию, когда какой-либо из компонентов обращается к ней
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();
    return $session;
});
/**
 * FileSystem component
 */
$di->setShared('fs', function () use ($config) {
    return new \Components\FileSystem();
});
/**
 * Excel component
 */
$di->setShared('excel', function () use ($config) {
    return new \Components\ConvertExcel();
});
/*
 * логи в файлы
 */
$di->setShared('logfile', function ($filename = null, $format = null) use ($config) {
    $format   = $format ?: $config->log->format;
    $filename = trim($filename ?: $config->log->filename, "\\/");
    $path     = $config->log->path;
    $formatter = new FormatterLine($format, $config->log->date);

    $logger    = new \Phalcon\Logger\Multiple();
    $logger->setFormatter($formatter);

    $appLog = new FileLogger($path . $filename);
    $appLog->setLogLevel($config->log->logLevel);

    $logger->push(
        $appLog
    );
    return $logger;
});

/**
 * Setting up the Viewer component
 */
$di->setShared('viewer', function () use ($di) {
    $viewer = new \Components\Viewer();
    $viewer->setDI($di);
    return $viewer;
});
$di->setShared('view',$di->get('viewer')->getView());
$di->setShared('assets',$di->get('viewer')->getAssets());

/*
 * Setup a base URI so that all generated URIs include the "tutorial" folder
 */
$di->setShared('url', function () use ($config) {
    $url = new UrlProvider();
    $url->setBaseUri('/');
    return $url;
});

$di->setShared('dispatcher', function () {
    $dispatcher = new Dispatcher();
    $dispatcher->setDefaultNamespace('Controllers');
    return $dispatcher;
});

/*
 * Router + ajax router
 */
$di->setShared('router', function () use ($config) {
    $router = require_once $config->application->routesPath;
    return $router;
});


$application = new \Phalcon\Mvc\Application();
$application->setDI($di);

return $application;