<?php
use Phalcon\Mvc\Router;

$router = new Router(false);
$router->removeExtraSlashes(true);

$router->add("/",
    [
        "controller" => 'index',
        "action" => 'index'
    ])->via(["GET"])->setName('homepage');

$router->add(
    "/{controller}/{action}/{param}",
    [
        "param" => 3
    ]
)->via(["GET"]);

$router->add("/{controller}/{action}",
    [

    ]
)->via(["GET"])->setName('action');


$router->add("/{controller}/:int",
    [
        "namespace" => "\\Controllers",
        "action" => 'detail',
        "id" => 2
    ]
)->via(["GET"])->setName('detail');


$router->add("/{controller}",
    [
        "action" => 'index'
    ]
)->via(["GET"])->setName('index');


include_once APP_PATH . '/config/Routes/ajax.php';

$router->mount(new AjaxRoutes());


$router->notFound(
    [
        "controller" => "error",
        "action"  => "route404"
    ]
);

return $router;