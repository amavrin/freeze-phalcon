<?php
use Phalcon\Mvc\Router\Group as RouterGroup;

class AjaxRoutes extends RouterGroup
{
    public function initialize()
    {
        $this->setPrefix('/ajax');

        $this->add(
            "/{action}/{controller}/{param}",
            [
                "namespace"    => '\\Controllers',
                "param" => 3
            ]
        )->setName('ajaxParam');

        $this->add(
            "/{action}/{controller}",
            [
                "namespace"    => '\\Controllers',
            ]
        )->setName('ajax');

    }
}