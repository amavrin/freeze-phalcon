<?php
$loader = new \Phalcon\Loader();

$rootpath = str_replace('\app','',APP_PATH);
define("ROOT_PATH",$rootpath);


/**
 * Регистрация пространств имен, связь с физическими путями из конфигурационного файла
 */
$loader->registerNamespaces(
    [
        "Models"                    => APP_PATH.$config->application->modelsDir,
        "Behaviors"                 => APP_PATH.$config->application->behaviorsDir,
        "Controllers"               => APP_PATH.$config->application->controllersDir,
        "Components"                => APP_PATH.$config->application->componentsDir,
        "Forms"                     => APP_PATH.$config->application->formsDir,
        "Access"                    => APP_PATH.$config->application->accessDir,
        "Log"                       => APP_PATH.$config->application->logDir,
    ]
);

$loader->registerDirs(
    array(
        APP_PATH.$config->application->modelsDir,
        APP_PATH.$config->application->behaviorsDir,
        APP_PATH.$config->application->controllersDir,
        APP_PATH.$config->application->componentsDir,
        APP_PATH.$config->application->formsDir,
        APP_PATH.$config->application->accessDir,
        APP_PATH.$config->application->logDir,
    )
);

$loader->register();
return $loader;