<?php
use Phalcon\Config,
	Phalcon\Logger;

ini_set("default_charset", "UTF-8");

$config = new \Phalcon\Config(
	[
		'database' => [
			'adapter'     	=> 'Mysql',
			'host'			=> 'localhost',
			'username'		=> 'root',
			'password'		=> '',
			'dbname'		=> 'system',
			'charset'     	=> 'utf8',
		],
		'files' => [
			'uploadDir' => APP_PATH.'\..\.runtime\files'
		],
		'application' => [
			'configPath'     		=> '/config',
			'accessDir'      		=> '/access/',
			'behaviorsDir' 	 		=> '/behaviors/',
			'controllersDir' 		=> '/controllers/',
			'modelsDir' 	 		=> '/models/',
			'componentsDir'  		=> '/components/',
			'logDir'     	 		=> '\\..\\.runtime\\logs\\',
			'pluginsDir'    		=> '/plugins/',
			'formsDir'       		=> '/forms/',
			'routesPath'     		=> APP_PATH.'/config/routes.php',
			'cacheDir'		 		=> APP_PATH.'\\..\\.runtime\\cache\\',
			'tmpDir'		 		=> APP_PATH.'\\..\\.runtime\\',
			'viewsDir' 	 	 		=> APP_PATH.'/views',
			'baseUri' 		 		=> '/app/'
		],
		'log' => [
			'path'     => APP_PATH.'\\..\\.runtime\\logs\\',
			'format'   => '%date% [%type%] %message%',
			'date'     => 'D j H:i:s',
			'logLevel' => Logger::DEBUG,
			'filename' => 'application.log',
		],
	]
);
return $config;