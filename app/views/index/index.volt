 <div id="content-container" style="padding-left: 0px;">
    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title" style="padding: 10px 0 0 25px;">
        <h1 class="page-header text-overflow">Hello</h1>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->
    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        <div class="row demo-nifty-panel">
            <div class="col-lg-6">
                <!--Panel with Tabs-->
                <!--===================================================-->
                <div class="panel panel-primary">
                    <!--Panel heading-->
                    <div class="panel-heading">
                        <div class="panel-control">
                            <!--Nav tabs-->
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#demo-tabs-box-1" aria-expanded="true">Получить пользователей</a>
                                </li>
                                <li class=""><a data-toggle="tab" href="#demo-tabs-box-2" aria-expanded="false">Получить склады</a>
                                </li>
                                <li class=""><a data-toggle="tab" href="#demo-tabs-box-3" aria-expanded="false">Получить продукты</a>
                                </li>
                                <li class=""><a data-toggle="tab" href="#demo-tabs-box-4" aria-expanded="false">Импорт продуктов</a>
                                </li>
                            </ul>

                        </div>
                        <h3 class="panel-title">Запросы</h3>
                    </div>
                    <!--Panel body-->
                    <div class="panel-body">
                        <!--Tabs content-->
                        <div class="tab-content">
                            <div id="demo-tabs-box-1" class="tab-pane fade active in">
                                <h4 class="text-thin">Получить пользователей</h4>
                                <form id="method1" meta-ajax="true">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Контрагент:</label>
                                            {{ filterContractorUserM1 }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="hidden" name="method" value="method1">
                                            <button class="btn btn-primary" id="button-method1" data-ul="#method1-ul" data-target="#page-content" type="submit">Отправить</button>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <ul class="list-group" id="method1-ul"></ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="demo-tabs-box-2" class="tab-pane fade">
                                <h4 class="text-thin">Получить склады</h4>
                                <form id="method2" meta-ajax="true">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Контрагент:</label>
                                            {{ filterContractorUserM2 }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="hidden" name="method" value="method2">
                                            <button class="btn btn-primary" id="button-method2" data-ul="#method2-ul" data-target="#page-content" type="submit">Отправить</button>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <ul class="list-group" id="method2-ul"></ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="demo-tabs-box-3" class="tab-pane fade">
                                <h4 class="text-thin">Получить продукты</h4>
                                <form id="method3" meta-ajax="true">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Контрагент:</label>
                                            {{ filterContractorUserM3 }}
                                        </div>
                                        <div class="col-sm-6" style="display:none">
                                            <label class="control-label">Склад:</label>
                                            {{ filterContractorStockM3 }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="hidden" name="method" value="method3">
                                            <button class="btn btn-primary" id="button-method3" data-ul="#method3-ul" data-target="#page-content" type="submit">Отправить</button>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <ul class="list-group" id="method3-ul"></ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="demo-tabs-box-4" class="tab-pane fade">
                                <h4 class="text-thin">Импорт продуктов</h4>
                                <form id="method4" meta-ajax="true">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Контрагент:</label>
                                            {{ filterContractorUserM4 }}
                                        </div>
                                        <div class="col-sm-6" style="display:none">
                                            <label class="control-label">Склад:</label>
                                            {{ filterContractorStockM4 }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="file" name="file">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="hidden" name="method" value="method4">
                                            <button class="btn btn-primary" id="button-method4" data-ul="#method4-ul" data-target="#page-content" type="submit">Отправить</button>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <ul class="list-group" id="method4-ul"></ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--===================================================-->
                <!--End of panel with tabs-->
            </div>
            <!--Panel with Table-->
            <!--===================================================-->
            <!--<div class="col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Table Results</h3>
                    </div>
                    <div class="panel-body">

                    </div>
                </div>
            </div>-->
            <!--===================================================-->
            <!--End of panel with table-->
        </div>
    </div>
    <!--===================================================-->
    <!--End page content-->
</div>