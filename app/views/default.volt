<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>Hello</title>
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">
        {{ assets.outputCss('cssHeader') }}
        {{ assets.outputJs('jsHeader') }}
        {{ assets.outputInlineJs('jsHeader') }}
    </head>
    <body>
        <div id="container" class="effect mainnav-lg">
            <div class="boxed">
                {{ content() }}
            </div>
        </div>
        {{ assets.outputJs('jsFooter') }}
    </body>
</html>