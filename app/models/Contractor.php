<?php
namespace Models;

use Phalcon\Mvc\Model;

class Contractor extends Model
{

	public $id;
    public $name;
    public $user_id;
    public $email;
    public $phone;
    public $date_create;
    public $date_update;

    public function initialize()
    {
        $this->hasMany('id', '\\Models\\Stock', 'contractor_id', ['alias' => 'Stock']);
        $this->hasManyToMany(
         "id",
         "\\Models\\RlUserContractor",
         "contractor_id", "user_id",
         "\\Models\\User",
         "id",
         ["alias" => "RlUser"]
        );
        $this->hasOne('user_id', '\\Models\\User', 'id', ['alias' => 'User']);
    }

}
