<?php
namespace Models;

use Phalcon\Mvc\Model;

class User extends Model
{

	public $id;
	public $login;
    public $date_create;
    public $date_update;
    public $firstname;
    public $lastname;
    public $patronymic;
	public $email;


    public function initialize()
    {
        $this->hasManyToMany(
            "id",
            "\\Models\\RlUserContractor",
            "user_id", "contractor_id",
            "\\Models\\Contractor",
            "id",
            ["alias" => "Contractor"]
        );
    }

    public function getFullName($format = '')
    {
        $full_name = "";
        switch ($format) {
            case "short":
                if($this->getLastname()) {
                    $full_name .= $this->getLastname();
                }
                if($this->getFirstname()) {
                    $full_name .= " ".mb_substr($this->getFirstname(), 0, 1, "UTF-8").".";
                }
                if($this->getPatronymic()) {
                    $full_name .= " ".mb_substr($this->getPatronymic(), 0, 1, "UTF-8").".";
                }
                break;
            default:
                if($this->getLastname()) {
                    $full_name .= $this->getLastname();
                }
                if($this->getFirstname()) {
                    $full_name .= " ".$this->getFirstname();
                }
                if($this->getPatronymic()) {
                    $full_name .= " ".$this->getPatronymic();
                }
                break;
        }
        return $full_name;
    }

    public function getFirstname()
    {
        if ($this->firstname) {
            return $this->firstname;
        } else {
            return false;
        }
    }

    public function getLastname()
    {
        if ($this->lastname) {
            return $this->lastname;
        } else {
            return false;
        }
    }

    public function getPatronymic()
    {
        if ($this->patronymic) {
            return $this->patronymic;
        } else {
            return false;
        }
    }
}
