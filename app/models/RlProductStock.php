<?php
namespace Models;

use Phalcon\Mvc\Model;

class RlProductStock extends Model
{

	public $id;
    public $product_id;
    public $stock_id;
    public $quantity;


    public function initialize()
    {
        $this->belongsTo("product_id", "\\Models\\Product", "id", array(
            'alias' => 'Product'
        ));
        $this->belongsTo("stock_id", "\\Models\\Stock", "id", array(
            'alias' => 'Stock'
        ));
    }

}
