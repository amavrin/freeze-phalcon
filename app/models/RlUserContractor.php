<?php
namespace Models;

use Phalcon\Mvc\Model;

class RlUserContractor extends Model
{

	public $id;
    public $user_id;
    public $contractor_id;


    public function initialize()
    {
        $this->belongsTo("user_id", "\\Models\\User", "id", array(
            'alias' => 'User'
        ));
        $this->belongsTo("contractor_id", "\\Models\\Contractor", "id", array(
            'alias' => 'Contractor'
        ));
    }

}
