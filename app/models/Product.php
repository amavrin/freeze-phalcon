<?php
namespace Models;

use \Phalcon\Mvc\Model,
    \Phalcon\Validation,
    \Phalcon\Validation\Validator\Uniqueness,
    \Phalcon\Validation\Validator\InclusionIn;


class Product extends Model
{

	public $id;
    public $name;
    public $artikul;
    public $brand;
    public $date_create;
    public $date_update;


    public function initialize()
    {
        $this->hasManyToMany(
            "id",
            "\\Models\\RlProductStock",
            "product_id", "stock_id",
            "\\Models\\Stock",
            "id",
            ["alias" => "RlStock"]
        );
    }

    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            [
                "artikul",
                "brand"
            ],
            new Uniqueness(
                [
                    "message" => "Такая комбинация artikul+brand уже существует",
                ]
            )
        );

        return $this->validate($validator);
    }
}
