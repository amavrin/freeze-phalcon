<?php
namespace Models;

use Phalcon\Mvc\Model;

class Stock extends Model
{

	public $id;
    public $contractor_id;
    public $name;
    public $city;
    public $region;
    public $region_code;
    public $country;
    public $phone;
    public $email;
    public $date_create;
    public $date_update;




    public function initialize()
    {
        $this->hasManyToMany(
            "id",
            "\\Models\\RlProductStock",
            "stock_id", "product_id",
            "\\Models\\Product",
            "id",
            ["alias" => "RlProduct"]
        );
    }

}
